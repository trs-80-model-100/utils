/* basenc.c -- generate TRS-80 Model 100 loading programs

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <err.h>
#include <errno.h>
#include <error.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VERSION "0.1"
#define YEAR "2024"

#define BUFFER_SIZE 4096 // must be a multiple of 2
#define DATA_COLS 26     // A-Z
#define CRLF "\r\n"

#define EINT 255

#define MAX(a, b)                                                              \
  ({                                                                           \
    typeof(a) _a = (a);                                                        \
    typeof(b) _b = (b);                                                        \
    _a > _b ? _a : _b;                                                         \
  })

#define AS_SIGNED_16(ptrexpr, offset) (*(int16_t *)(&(ptrexpr)[(offset)]))

static const int8_t hex[256] = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0,  1,  2,  3,  4,  5,  6,  7,  8,
    9,  -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

static const char *program_name;
static const char *optstring = "e:hio:rs:v";
static const struct option options[] = {
    {"end", required_argument, NULL, 'e'},
    {"help", no_argument, NULL, 'h'},
    {"ihex", no_argument, NULL, 'i'},
    {"origin", required_argument, NULL, 'o'},
    {"run", no_argument, NULL, 'r'},
    {"save", required_argument, NULL, 's'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}};

typedef struct options {
  uint8_t ihex : 1;
  uint8_t save : 1;
  uint8_t run : 1;
  uint16_t origin;
  uint16_t entry;
  char *filename;
} options_t;

typedef struct cursor {
  uint8_t col;
  uint16_t row;
} cursor_t;

typedef struct context {
  int infd;
  int outfd;
  options_t options;
  cursor_t cursor;
  uint16_t length;
} context_t;

static void parse_options(int argc, char *argv[], options_t *opts);
static void print_help(FILE *restrict out);
static void print_version(void);

/* Generate loader program for `infd', write it to `outfd' and return the number
 * of 16bit double words written.
 */
static int32_t transform_raw(context_t *context);
static int32_t transform_ihex(context_t *context);

static int write_dword(context_t *context, int16_t dword);

int main(int argc, char *argv[]) {
  program_name = argv[0];
  context_t context = {
      .infd = STDIN_FILENO, .outfd = STDOUT_FILENO, .cursor.row = 1};

  parse_options(argc, argv, &context.options);

  const int32_t dwords =
      context.options.ihex ? transform_ihex(&context) : transform_raw(&context);
  const uint16_t end = context.options.origin + context.length;
  if (dwords < 0 && errno == EINT)
    exit(EXIT_FAILURE);
  if (dwords < 0)
    err(EXIT_FAILURE, "transformation failed");
  if (dwords == 0)
    error(EXIT_FAILURE, 0, "input is empty");
  if (context.options.origin < 0x7FFF)
    error(0, 0, "loading data to ROM (addresses below 7FFF) does nothing");
  if (end >= 0xF5F0)
    error(0, 0,
          "loading data to addresses above F5F0 might interfere with "
          "operation system");

  if (dprintf(context.outfd,
              "%uDEFINTD,I,P"
              ":D=0"
              ":P=VARPTR(D)"
              ":FORI=0TO%d"
              ":READD"
              ":POKE%u+I*2,PEEK(P)"
              ":POKE%u+I*2+1,PEEK(P+1)"
              ":NEXT" CRLF,
              context.cursor.row++, dwords - 1, context.options.origin,
              context.options.origin) < 0)
    err(-1, "cannot write output");

  if (context.options.save) {
    if (context.options.entry < context.options.origin)
      error(0, 0, "entry address is lower than origin");
    if (context.options.entry == 0)
      error(0, 0, "entry address is set to zero");
    if (dprintf(context.outfd, "%uSAVEM\"%s\",%d,%d,%d" CRLF,
                context.cursor.row++, context.options.filename,
                context.options.origin, end, context.options.entry) < 0)
      err(-1, "cannot write output");
  }
  if (context.options.run) {
    if (dprintf(context.outfd, "%uCALL%d" CRLF, context.cursor.row++,
                context.options.entry) < 0)
      err(-1, "cannot write output");
  }
  return EXIT_SUCCESS;
}

static void parse_options(int argc, char *argv[], options_t *opts) {
  int extra_opt = 0, end = 0, opt, origin = 0;
  while ((opt = getopt_long(argc, argv, optstring, options, NULL)) != -1) {
    switch (opt) {
    case 'e':
      opts->entry = strtol(optarg, NULL, 0);
      if (errno != 0)
        err(-1, "end must be a valid integer");
      end = 1;
      break;
    case 'h':
      print_help(stdout);
      exit(EXIT_SUCCESS);
      break;
    case 'i':
      opts->ihex = 1;
      break;
    case 'o':
      opts->origin = strtol(optarg, NULL, 0);
      if (errno != 0)
        err(-1, "origin must be a valid integer");
      origin = 1;
      break;
    case 'r':
      opts->run = 1;
      break;
    case 's':
      opts->save = 1;
      opts->filename = optarg;
      break;
    case 'v':
      print_version();
      exit(EXIT_SUCCESS);
      break;
    case '?':
      print_help(stderr);
      exit(EXIT_FAILURE);
      break;
    default:
      extra_opt = 1;
      break;
    }
  }

  if (extra_opt || optind < argc) {
    if (argv[optind])
      error(0, 0, "extra option: %s", argv[optind]);
    print_help(stderr);
    exit(EXIT_FAILURE);
  }

  if (opts->ihex && origin)
    error(0, 0, "using IHEX origin: origin option will be ignored");

  if (opts->ihex && end)
    error(0, 0,
          "entry point address in IHEX will override the one provided with end "
          "option if present");

  if (!opts->ihex && !origin)
    error(0, 0,
          "loading a raw binary without specifing the origin option is not "
          "recommended");
}

static void print_help(FILE *restrict out) {
  fprintf(out, "Usage: %s [OPTION]...\n", program_name);
  fputs("Generate a TRS-80 Model 100 loading program for a given binary "
        "file.\n"
        "Binary data is read from STDIN and BASIC program is written to "
        "STDOUT.\n\n"
        "  -e, --end ADDR     entry point address (use prefix 0x for base 16)\n"
        "  -h, --help         display this help and exit\n"
        "  -i, --ihex         treat input format as ihex\n"
        "  -o, --origin ADDR  target address (use prefix 0x for base 16)\n"
        "  -r, --run          run the program after loading it into memory\n"
        "  -s, --save NAME    save the program after loading it into memory\n"
        "  -v, --version      display version information and exit\n",
        out);
}

static void print_version(void) {
  printf("%s (BASIC Encode) %s\n", program_name, VERSION);
  printf("Copyright (C) %s Cyrill Schenkel\n\n", YEAR);
  puts("License GPLv3+: GNU GPL version 3 or later "
       "<https://gnu.org/licenses/gpl.html>.\n"
       "This is free software: you are free to change and redistribute it.\n"
       "There is NO WARRANTY, to the extent permitted by law.\n\n"
       "Written by Cyrill Schenkel");
}

static int32_t transform_raw(context_t *context) {
  const int infd = context->infd, outfd = context->outfd;
  cursor_t *cursor = &context->cursor;
  uint8_t buffer[BUFFER_SIZE];
  int length = 0;

  while ((length = read(infd, buffer, BUFFER_SIZE)) != 0) {
    if (length < 0)
      return -1;

    context->length += length;

    // ensure the the input buffer contains an even number of bytes
    if (length < BUFFER_SIZE)
      buffer[length] = 0;

    for (int i = 0; i < length; i += 2)
      if (write_dword(context, AS_SIGNED_16(buffer, i)) < 0)
        return -1;
  }
  if (cursor->col > 0 && write(outfd, CRLF, 2) < 0)
    return -1;

  return MAX(0, cursor->row - 2) * DATA_COLS + cursor->col;
}

static int32_t transform_ihex(context_t *context) {
  const int infd = context->infd, outfd = context->outfd;
  cursor_t *cursor = &context->cursor;
  uint8_t buffer[BUFFER_SIZE];
  int length = 0, payload = 0, recnum = 0;
  uint8_t reccol = 0, reclen = 0, recsum = 0, rectype = 0, nibble = 3;
  uint16_t addr = 0, sum = 0;
  int16_t dword = 0;
  int32_t dwords = 0;

  while ((length = read(infd, buffer, BUFFER_SIZE)) != 0) {
    if (length < 0)
      return -1;

    for (int i = 0; i < length; i++) {
      const uint8_t c = buffer[i];

      if (c == '\n' || c == '\r') {
        sum = ((~sum & 0xFF) + 1) & 0xFF; // two's complement
        if (payload && recsum != sum) {
          errno = EINT;
          fprintf(stderr, "checksum mismatch: %X != %X", (uint8_t)sum, recsum);
          return -1;
        }
        reccol = 0;
        recsum = 0;
        continue;
      }

      // skip header
      if (!payload && reccol > 0)
        continue;

      switch (reccol) {
      case 0:
        if (payload && c != ':' && c != '\n' && c != '\r') {
          errno = EINT;
          fprintf(stderr, "unexpected character in column 0: %c (%X)\n", c, c);
          return -1;
        }
        if (!payload)
          payload = c == ':';
        else if (c == ':')
          recnum++;
        break;
      case 1:
        reclen = hex[c] << 4;
        break;
      case 2:
        reclen += hex[c];
        sum = reclen;
        break;
      case 3:
        addr = hex[c] << 12;
        break;
      case 4:
        addr += hex[c] << 8;
        sum += addr >> 8;
        break;
      case 5:
        addr += hex[c] << 4;
        break;
      case 6:
        addr += hex[c];
        if (recnum == 0)
          context->options.origin = addr;
        sum += addr & 0xFF;
        break;
      case 7:
        rectype = hex[c] << 4;
        break;
      case 8:
        rectype += hex[c];
        sum += rectype;
        if (rectype == 0)
          context->length += reclen;
        if (rectype == 1)
          context->options.entry = addr;
        break;
      default:
        if ((reccol - 9) / 2 >= reclen) {
          recsum += hex[c] << (reccol & 1) * 4;
          break;
        }
        if (rectype != 0)
          break;
        dword += hex[c] << ((nibble + 2) & 0b11) * 4;
        if (nibble == 2)
          sum += dword & 0xFF;
        if (nibble == 0) {
          write_dword(context, dword);
          sum += dword >> 8;
          dword = 0;
          nibble = 4;
          dwords++;
        }
        nibble--;
        break;
      }
      sum &= 0xFF;
      reccol++;
    }
  }
  if (nibble == 1) {
    write_dword(context, dword);
    dwords++;
  }
  if (cursor->col > 0 && write(outfd, CRLF, 2) < 0)
    return -1;

  return dwords;
}

static int write_dword(context_t *context, int16_t dword) {
  cursor_t *cursor = &context->cursor;
  if (cursor->col == 0) {
    if (dprintf(context->outfd, "%uDATA%d", cursor->row, dword) < 0)
      return -1;
    cursor->col = 1;
    cursor->row++;
  } else {
    if (dprintf(context->outfd, ",%d", dword) < 0)
      return -1;
    cursor->col++;
    if (cursor->col >= DATA_COLS) {
      cursor->col = 0;
      if (write(context->outfd, CRLF, 2) < 0)
        return -1;
    }
  }
  return 0;
}
