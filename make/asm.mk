# Define CO_HEAD variable to generate output including .CO header fields.
ifdef CO_HEAD
AS_ARGS = -i '.define CO_HEAD "1"'
endif

%.do: %.hex
	basenc -i -s $(basename $@) > $@ < $<

# Define ORIGIN variable to generate loader for .bin file
ifdef ORIGIN
%.do: %.bin
	basenc -o $(ORIGIN) > $@ < $<
endif

%.bin: %.hex
	objcopy -I ihex -O binary $< $@

%.hex: %.rel
	aslink -i $<

%.rel: %.asm
	as8085 $(AS_ARGS) -o $<

%.lst: %.asm
	as8085 $(AS_ARGS) -l $<
