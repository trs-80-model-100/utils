{ stdenv
, lib
}:

stdenv.mkDerivation {
  pname = "m100-utils";
  version = "1.0";

  src = ./.;

  buildInputs = [];

  enableParallelBuilding = true;

  doCheck = true;

  installPhase = ''
    mkdir -p $out/make $out/bin
    cp make/* $out/make
    cp basenc $out/bin
  '';

  meta = with lib; {
    description = "TRS-80 Model 100 Utils";
    platforms = platforms.linux;
  };
}
