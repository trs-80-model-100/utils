# TRS-80 Model 100 Development Utilities

## BASIC Encode

The `basenc` utility facilitates copying binary files to the TRS-80 Model 100.
For this purpose it generates BASIC code which can then be loaded using the
serial port on the Model 100.

```basic
LOAD "COM:38N1D,10"
```

Raw binaries and Intel HEX are supported for now.

## Makefiles

The `make` directory contains Makefiles with common rules. These Makefiles can
be included in other Makefiles with the include directive:

```make
include $(M100_UTILS_MK)/asm.mk
```

## License

See [LICENSE](LICENSE).
