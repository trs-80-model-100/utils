{
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        m100-utils = pkgs.callPackage ./package.nix {};
      in
      {
        packages.default = m100-utils;

        devShells.default = with pkgs; mkShell {
          packages = [
            m100-utils
          ];

          buildInputs = [
          ];

          # Example: Include makefiles using an environemtn variable.
          #
          # Put the following line at the start of your Makefile
          #
          #   include $(M100_UTILS_MK)/basic.mk
          #
          M100_UTILS_MK = "${m100-utils}/make";
        };
      }
    );
}
