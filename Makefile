include make/basic.mk
include make/asm.mk

CFLAGS = -Wall -Wextra

all: basenc

basenc: basenc.o
	$(CC) $(CFLAGS) -o $@ $<

.PHONY: clean check ChangeLog

ChangeLog:
	build-aux/gitlog-to-changelog > ChangeLog

clean:
	rm -f test/*.do
	rm -f basenc
	rm -f *.o

check: test/hello.do
	diff -u test/hello.do test/hello.exp
	./basenc -o 0xEA60 < test/hello.bin > test/hello.a.do
	./basenc -i < test/hello.hex > test/hello.b.do
	diff -u test/hello.a.do test/hello.b.do
	rm -f test/*.do

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
